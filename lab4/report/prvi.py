import cv2

img = cv2.imread('C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab4\\slike\\lenna.bmp',0)
blurred = cv2.GaussianBlur(img, (5,5), 0)

#lower=0,upper=255
lennaedge = cv2.Canny(blurred,0,255)
cv2.imwrite('C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab4\\slike\\lenna0_255.jpg',lennaedge)

#lower=128,upper=128
lennaedge = cv2.Canny(blurred,128,128)
cv2.imwrite('C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab4\\slike\\lenna128.jpg',lennaedge)

#lower=128,upper=128
lennaedge = cv2.Canny(blurred,255,255)
cv2.imwrite('C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab4\\slike\\lenna255.jpg',lennaedge)

