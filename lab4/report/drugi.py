import numpy as np
import cv2
from matplotlib import pyplot as plt
import os

def auto_canny(image, sigma=0.33):
        # compute the median of the single channel pixel intensities
        v = np.median(image)

        # apply automatic Canny edge detection using the computed median
        # lower = int(max(0, (1.0 - sigma) * v))
        # upper = int(min(255, (1.0 + sigma) * v))
        # lower = int(max(128, (1.0 - sigma) * v))
        # upper = int(min(128, (1.0 + sigma) * v))
        lower = int(max(255, (1.0 - sigma) * v))
        upper = int(min(255, (1.0 + sigma) * v))
        edged = cv2.Canny(image, lower, upper)
        print ("Median lower: %r." % lower)
        print ("Median upper: %r." % upper)
        # return the edged image
        return edged

#image = cv2.imread("D:/osirv_labs/lab4/slike/lenna.bmp",0)
#image = cv2.imread("D:/osirv_labs/lab4/slike/airplane.bmp",0)
#image = cv2.imread("D:/osirv_labs/lab4/slike/barbara.bmp",0)
#image = cv2.imread("D:/osirv_labs/lab4/slike/boats.bmp",0)
image = cv2.imread("C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab4\\slike\\lenna.bmp",0)
auto = auto_canny(image)

plt.subplot(121),plt.imshow(image, cmap='gray')
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(auto,  'gray')
plt.title('Edges'), plt.xticks([]), plt.yticks([])
plt.show()


path = r'C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab4\\slike'
os.chdir(path)

###### airplane
#cv2.imwrite("airplane_0-255.jpg", auto)
#cv2.imwrite("airplane_128-128.jpg", auto)
#cv2.imwrite("airplane_255-255.jpg", auto)

###### barbara
#cv2.imwrite("barbara_0-255.jpg", auto)
#cv2.imwrite("barbara_128-128.jpg", auto)
#cv2.imwrite("barbara_255-255.jpg", auto)

###### boats
#cv2.imwrite("boats_0-255.jpg", auto)
#cv2.imwrite("boats_128-128.jpg", auto)
#cv2.imwrite("boats_255-255.jpg", auto)

###### pepper
#cv2.imwrite("pepper_0-255.jpg", auto)
#cv2.imwrite("pepper_128-128.jpg", auto)
cv2.imwrite("pepper_255-255.jpg", auto)
