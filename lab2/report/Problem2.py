import matplotlib.pyplot as plt
import numpy as np
import cv2

image = cv2.imread("C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab2\\slike\\baboon.bmp")
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

kernel = np.array (([-1, 1, 0],
                    [4, -4, 1],
                    [0, 1, 0]), np.float32)
output = cv2.filter2D(image, -1, kernel)

plt.subplot(1, 2, 1)
plt.imshow(image)
plt.title("Origigi")

plt.subplot(1, 2, 2)
plt.imshow(output)
plt.title("Konvolucija")

cv2.imwrite("C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab2\\slike\\baboonkonvo.bmp", output)

plt.show()