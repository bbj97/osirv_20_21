import matplotlib.pyplot as plt
import cv2

image1 = cv2.imread("C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab2\\slike\\baboon.bmp")
image2 = cv2.imread("C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab2\\slike\\lenna.bmp")
image3 = cv2.imread("C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab2\\slike\\airplane.bmp")

image1 = cv2.cvtColor(image1, cv2.COLOR_BGR2RGB)
image2 = cv2.cvtColor(image2, cv2.COLOR_BGR2RGB)
image3 = cv2.cvtColor(image3, cv2.COLOR_BGR2RGB)

ret, baboon_th1 = cv2.threshold(image1, 63, 255, cv2.THRESH_BINARY)
plt.imshow(baboon_th1)
plt.title("Threshold_63")
plt.show()

ret, baboon_th2 = cv2.threshold(image1, 127, 255, cv2.THRESH_BINARY)
plt.imshow(baboon_th2)
plt.title("Threshold_127")
plt.show()

ret, baboon_th3 = cv2.threshold(image1, 191, 255, cv2.THRESH_BINARY)
plt.imshow(baboon_th3)
plt.title("Threshold_191")
plt.show()


ret, lenna_th1 = cv2.threshold(image2, 63, 255, cv2.THRESH_BINARY)
plt.imshow(lenna_th1)
plt.title("Threshold_63")
plt.show()

ret, lenna_th2 = cv2.threshold(image2, 127, 255, cv2.THRESH_BINARY)
plt.imshow(lenna_th2)
plt.title("Threshold_127")
plt.show()

ret, lenna_th3 = cv2.threshold(image2, 191, 255, cv2.THRESH_BINARY)
plt.imshow(lenna_th3)
plt.title("Threshold_191")
plt.show()


ret, airplane_th1 = cv2.threshold(image3, 63, 255, cv2.THRESH_BINARY)
plt.imshow(airplane_th1)
plt.title("Threshold_63")
plt.show()

ret, airplane_th2 = cv2.threshold(image3, 127, 255, cv2.THRESH_BINARY)
plt.imshow(airplane_th2)
plt.title("Threshold_127")
plt.show()

ret, airplane_th3 = cv2.threshold(image3, 191, 255, cv2.THRESH_BINARY)
plt.imshow(airplane_th3)
plt.title("Threshold_191")
plt.show()

cv2.imwrite("C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab2\\slike\\baboon_63_thresh.png", baboon_th1)
cv2.imwrite("C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab2\\slike\\baboon_127_thresh.png", baboon_th2)
cv2.imwrite("C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab2\\slike\\baboon_191_thresh.png", baboon_th3)

cv2.imwrite("C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab2\\slike\\lenna_63_thresh.png", lenna_th1)
cv2.imwrite("C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab2\\slike\\lenna_127_thresh.png", lenna_th2)
cv2.imwrite("D:/osirv_labs/osirv_20_21/lab2/slike/problem4/lenna_191_thresh.png", lenna_th3)

cv2.imwrite("C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab2\\slike\\airplane_63_thresh.png", airplane_th1)
cv2.imwrite("C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab2\\slike\\airplane_127_thresh.png", airplane_th2)
cv2.imwrite("C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab2\\slike\\airplane_191_thresh.png", airplane_th3)