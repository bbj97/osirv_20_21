# -*- coding: utf-8 -*-
"""
Created on Fri Jan  8 18:02:01 2021

@author: Bljeona
"""

import numpy as np
import cv2

cap = cv2.VideoCapture('C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab5\\lab5_road.mp4')

# define the codec and create VideoWriter object
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab5\\output.avi',fourcc, 20.0, (640,480))

while(cap.isOpened()):
    ret, frame = cap.read()
    if ret==True:
        frame = cv2.flip(frame,-180)

        # write the flipped frame
        out.write(frame)

        # Display the resulting frame
        cv2.imshow('Frame',frame)
        k = cv2.waitKey(30) & 0xff
        if k == 27:
             break
# release everything if job is finished
cap.release()
out.release()
cv2.destroyAllWindows()
