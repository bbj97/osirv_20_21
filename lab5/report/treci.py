# -*- coding: utf-8 -*-
"""
Created on Fri Jan  8 18:30:31 2021

@author: Bljeona
"""
import cv2

vid = cv2.VideoCapture(0)
while(True): 
    ret, frame = vid.read() 
    cv2.imshow('frame', frame) 
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break
  
vid.release() 
cv2.destroyAllWindows()