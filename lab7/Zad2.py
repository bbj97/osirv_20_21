import cv2 as cv
import numpy as np

img = cv.imread("C:\\Users\\Bljeona\\Documents\\OSIRV\\osirv_20_21\\lab7\\slike\\lenna.bmp")
# convert to HSV
hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)

# TODO: Threshold img's hue range so that only green peppers are showing, and all other pixels are black
# Do this by thresholding a HSV image to green-yellow-ish hue range, and S and V components between 30 and 250.
# Use Google to find out which hue values to use for as the upper and lower bound, employ some trial and error
# to fine-tune the range so that you can see green peppers.

lower = np.array([40, 40, 40])
upper = np.array([100, 250, 250])

mask=cv.inRange(hsv, lower, upper)

img=cv.bitwise_and(img, img, mask=mask)
cv.imwrite("C:\\Users\\Bljeona\\Documents\\osirvv\\osirv_20_21\\lab7\\slike\\imgthreshlenna.bmp", img)


cv.imshow("Thresholded", img)
cv.waitKey(0)
cv.destroyAllWindows()
